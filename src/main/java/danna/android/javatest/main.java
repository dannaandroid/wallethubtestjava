package danna.android.javatest;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class main {

  public static void main(String[] args) {
    getLogs();
  }

  private static void getLogs() {
    String fileString = "C:/access.log";
    try {
      //********(1) first method for persist data*****//

      //List<Log> logs = fileStreamUsingBufferedReader(fileString);
      //save(logs);

      //********(2) second method for get data*****//

      getRequests("2017-01-01 00:01:17.472", "daily", 50);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  private static List<Log> fileStreamUsingBufferedReader(String fileName) throws IOException {
    BufferedReader reader = Files.newBufferedReader(Paths.get(fileName));

    return reader.lines().map(elem -> {
      String[] line = elem.split("\\|");
      return Log.builder().dateLog(dateStringToDate(line[0])).ip(line[1]).request(line[2]).status
          (line[3]).userAgent(line[4]).build();
    }).collect(Collectors.toList());
  }

  private static DateTime dateStringToDate(String dateString) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS");
    return DateTime.parse(dateString, formatter);
  }

  private static void save(List<Log> logs) throws SQLException, IOException {
    String url = "jdbc:mysql://127.0.0.1:3306/wallethub?useUnicode=true&characterEncoding=utf8" +
        "&user=root&password=dindon2009&serverTimezone=UTC";
    JdbcPooledConnectionSource connectionSource = new JdbcPooledConnectionSource(url);
    Dao<Log, Long> logDao = DaoManager.createDao(connectionSource, Log.class);
    for (Log log : logs) {
      logDao.create(log);
    }
    connectionSource.close();
  }

  private static void getRequests(String startDate, String duration, int threshold) throws SQLException {
    String url = "jdbc:mysql://127.0.0.1:3306/wallethub?useUnicode=true&characterEncoding=utf8" +
        "&user=root&password=dindon2009&serverTimezone=UTC";
    JdbcPooledConnectionSource connectionSource = new JdbcPooledConnectionSource(url);

    Dao<Log, Long> logDao = DaoManager.createDao(connectionSource, Log.class);

    DateTime startDateRequest = dateStringToDate(startDate);
    DateTime endDateRequest = duration.equals("hourly") ? startDateRequest.plusHours(1) :
        duration.equals("daily") ? startDateRequest.plusDays(1) : startDateRequest;

    List<Log> logs = logDao.queryBuilder()
        .where().between("dateLog", startDateRequest, endDateRequest)
        .query();

    logs.forEach(log -> System.out.printf(log.getIp()));
  }
}
