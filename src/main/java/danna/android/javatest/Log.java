package danna.android.javatest;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@DatabaseTable(tableName = "logs")
public class Log {
  @DatabaseField
  private DateTime dateLog;
  @DatabaseField
  private String ip;
  @DatabaseField
  private String request;
  @DatabaseField
  private String status;
  @DatabaseField
  private String userAgent;
}
